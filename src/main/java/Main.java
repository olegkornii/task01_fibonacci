import Exeptions.TooBigIntExeption;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try (Fibonacci fibonacci = new Fibonacci()) {


            do {
                System.out.println("Введіть додатній або відємний індекс");
                System.out.println("Для виходу введіть '0'");
                System.out.println("__________________________________");

                try {
                    Scanner scanner = new Scanner(System.in);
                    final int n = scanner.nextInt();


                    if (n == 0) {
                        System.out.println("Ви вийшли)");
                        break;

                    }

                    System.out.println(fibonacci.getFibonacciList(n));

                } catch (InputMismatchException e) {

                    System.out.println("Це не число)))");
                }
                catch (TooBigIntExeption e){
                    e.showMessage();
                }

            } while (true);
        } catch (Exception e) {
            System.out.println("Щось пішло не так((");
        }
    }
}
