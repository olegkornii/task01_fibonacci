import Exeptions.TooBigIntExeption;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


class Fibonacci implements AutoCloseable {

    List getFibonacciList(final int n) throws TooBigIntExeption {

        final int absN = Math.abs(n);
        ArrayList<Integer> fibArr;
        if (absN > 5_000) {
            throw new TooBigIntExeption();

        }
        //відємний ряд
        if (n < 0) {

            fibArr = new ArrayList<Integer>(Collections.nCopies(absN, 0));

            if (n == -1) {
                fibArr.set(0, 1);
                return fibArr;
            }

            fibArr.set(absN - 2, -1);
            fibArr.set(absN - 1, 1);

            for (int i = absN - 3; i >= 0; i--) {
                int f1 = fibArr.get(i + 1);
                int f2 = fibArr.get(i + 2);
                fibArr.set(i, (f2 - f1));

            }
            //Додатній ряд
        } else {
            fibArr = new ArrayList<Integer>();
            fibArr.add(0, 0);
            fibArr.add(1, 1);

            for (int i = 2; i <= n; i++) {
                int f1 = fibArr.get(i - 1);
                int f2 = fibArr.get(i - 2);
                fibArr.add(i, (f1 + f2));

            }
        }
        return fibArr;
    }

    public void close() throws Exception {
        System.out.println("Closed!");
    }
}
